pub mod vec3 {
    use crate::utils::utils::{random_double, random_double_range};

    pub type Vec3 = [f32; 3];

    #[inline]
    pub fn add_vector(v1: &mut Vec3, v2: &Vec3) {
        v1[0] += v2[0];
        v1[1] += v2[1];
        v1[2] += v2[2];
    }

    #[inline]
    pub fn add_vectorf(v1: &mut Vec3, f: f32) {
        add_vector(v1, &[f, f, f]);
    }

    #[inline]
    pub fn sub_vector(v1: &mut Vec3, v2: &Vec3) {
        v1[0] -= v2[0];
        v1[1] -= v2[1];
        v1[2] -= v2[2];
    }

    #[inline]
    pub fn sub_vectorf(v1: &mut Vec3, f: f32) {
        sub_vector(v1, &[f, f, f]);
    }

    #[inline]
    pub fn mul_vector(v1: &mut Vec3, v2: &Vec3) {
        v1[0] *= v2[0];
        v1[1] *= v2[1];
        v1[2] *= v2[2];
    }

    #[inline]
    pub fn mul_vectorf(v1: &mut Vec3, f: f32) {
        mul_vector(v1, &[f, f, f]);
    }

    #[inline]
    pub fn div_vector(v1: &mut Vec3, v2: &Vec3) {
        v1[0] /= v2[0];
        v1[1] /= v2[1];
        v1[2] /= v2[2];
    }

    #[inline]
    pub fn div_vectorf(v1: &mut Vec3, f: f32) {
        div_vector(v1, &[f, f, f]);
    }

    #[inline]
    pub fn length(v: &Vec3) -> f32 {
        let l = length_squared(v).sqrt();
        l
    }

    #[inline]
    pub fn length_squared(v: &Vec3) -> f32 {
        ((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2])) as f32
    }

    #[inline]
    pub fn mul_by_neg_one(v: &mut Vec3) {
        mul_vector(v, &[-1.0, -1.0, -1.0]);
    }

    #[inline]
    pub fn dot(v1: &Vec3, v2: &Vec3) -> f32 {
        (v1[0] * v2[0]) + (v1[1] * v2[1]) + (v1[2] * v2[2])
    }

    #[inline]
    pub fn cross(v1: &Vec3, v2: &Vec3) -> Vec3 {
        [
            v1[1] * v2[2] - v1[2] * v2[1],
            v1[2] * v2[0] - v1[0] * v2[2],
            v1[0] * v2[1] - v1[1] * v2[0],
        ]
    }

    #[inline]
    pub fn unit_vector(v: &Vec3) -> Vec3 {
        let l = length(v);
        let mut v2 = *v;

        div_vector(&mut v2, &[l, l, l]);

        v2
    }

    #[inline]
    pub fn vec_from(f: f32) -> Vec3 {
        [f, f, f]
    }

    #[inline]
    fn vec_random() -> Vec3 {
        [random_double(), random_double(), random_double()]
    }

    #[inline]
    fn vec_random_range(min: f32, max: f32) -> Vec3 {
        [
            random_double_range(min, max),
            random_double_range(min, max),
            random_double_range(min, max),
        ]
    }

    pub fn vec_in_unit_sphere() -> Vec3 {
        loop {
            let p: Vec3 = vec_random();
            if length_squared(&p) >= 1.0 {
                continue;
            }

            return p;
        }
    }
}
