pub mod camera {
    use crate::{
        ray::ray::Ray,
        vec3::vec3::{add_vector, div_vectorf, mul_vectorf, sub_vector, vec_from, Vec3},
    };

    pub struct Camera {
        pub aspect_ratio: f32,
        pub viewport_height: f32,
        pub viewport_width: f32,
        pub focal_length: f32,

        pub origin: Vec3,
        pub horizontal: Vec3,
        pub vertical: Vec3,
        pub lower_left_corner: Vec3,
    }

    impl Default for Camera {
        fn default() -> Self {
            let ar: f32 = 16.0 / 9.0;
            let vh: f32 = 2.0;
            let vw: f32 = ar * vh;
            let fl: f32 = 1.0;
            let ori: Vec3 = vec_from(0.0);

            let mut llc = ori;

            let h: Vec3 = [vw, 0.0, 0.0];
            let v: Vec3 = [0.0, vh, 0.0];

            let mut h2 = h;
            let mut v2 = v;
            div_vectorf(&mut h2, 2.0);
            div_vectorf(&mut v2, 2.0);

            sub_vector(&mut llc, &h2);
            sub_vector(&mut llc, &v2);

            sub_vector(&mut llc, &[0.0, 0.0, fl]);

            Camera {
                aspect_ratio: ar,
                viewport_height: vh,
                viewport_width: vw,
                focal_length: fl,
                origin: ori,
                horizontal: h,
                vertical: v,
                lower_left_corner: llc,
            }
        }
    }

    pub fn get_ray_from_cam(cam: &Camera, u: f32, v: f32) -> Ray {
        let mut hu = cam.horizontal;
        mul_vectorf(&mut hu, u);

        let mut vv = cam.vertical;
        mul_vectorf(&mut vv, v);

        let mut dir: Vec3 = cam.lower_left_corner;

        add_vector(&mut dir, &hu);
        add_vector(&mut dir, &vv);
        sub_vector(&mut dir, &cam.origin);

        Ray {
            origin: cam.origin,
            direction: dir,
        }
    }
}
