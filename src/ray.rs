pub mod ray {
    use crate::vec3::vec3::{add_vector, mul_vectorf, Vec3};

    pub struct Ray {
        pub origin: Vec3,
        pub direction: Vec3,
    }

    pub fn at(r: &Ray, t: f32) -> Vec3 {
        let mut mod_direction: Vec3 = r.direction;
        let mut point: Vec3 = r.origin;

        mul_vectorf(&mut mod_direction, t);

        add_vector(&mut point, &mod_direction);

        point
    }
}
