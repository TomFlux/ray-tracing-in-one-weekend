pub mod utils {
    use rand::distributions::{Distribution, Uniform};
    use std::f32::consts::PI;

    #[inline]
    pub fn degrees_to_radians(deg: f32) -> f32 {
        (deg * PI) / 180.0
    }

    #[inline]
    pub fn random_double() -> f32 {
        let mut rng = rand::thread_rng();
        let die = Uniform::from(0.0..1.0);

        die.sample(&mut rng)
    }

    #[inline]
    pub fn random_double_range(min: f32, max: f32) -> f32 {
        min + (max - min) * random_double()
    }

    #[inline]
    pub fn clamp(x: f32, min: f32, max: f32) -> f32 {
        if x < min {
            return min;
        } else if x > max {
            return max;
        }

        x
    }
}
