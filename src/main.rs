mod camera;
mod colour;
mod hittable;
mod ray;
mod utils;
mod vec3;

use std::f32::INFINITY;

use colour::colour::*;
use hittable::hittable::{empty_hitrecord, hits};
use ray::ray::Ray;
use vec3::vec3::*;

use crate::{
    camera::camera::{get_ray_from_cam, Camera},
    hittable::hittable::{Hittables, Sphere},
    utils::utils::random_double,
};

const ASPECT_RATIO: f32 = 16.0 / 9.0;
const IMAGE_WIDTH: usize = 400;
const IMAGE_HEIGHT: usize = (IMAGE_WIDTH as f32 / ASPECT_RATIO) as usize;
const SAMPLES_PER_PIXEL: usize = 10;
const MAX_DEPTH: u8 = 100;

fn ray_colour(r: &Ray, world: &Hittables, depth: u8) -> Vec3 {
    let mut rec = empty_hitrecord();

    if depth <= 0 {
        return vec_from(0.0);
    }

    if hits(world, r, 0.0001, INFINITY, &mut rec) {
        let mut target: Vec3 = rec.p;
        add_vector(&mut target, &rec.normal);
        add_vector(&mut target, &vec_in_unit_sphere());
        sub_vector(&mut target, &rec.p);

        let r = Ray {
            origin: rec.p,
            direction: target,
        };
        let mut new_colour: Vec3 = ray_colour(&r, world, depth - 1);
        mul_vectorf(&mut new_colour, 0.5);

        return new_colour;
    }

    let unit_dir = unit_vector(&r.direction);
    let t = 0.5 * (unit_dir[1] + 1.0);
    let tn = 1.0 - t;

    let mut v = vec_from(tn);
    mul_vectorf(&mut v, 1.0);

    let mut v2 = vec_from(t);
    mul_vector(&mut v2, &[0.5, 0.7, 1.0]);

    add_vector(&mut v, &v2);

    v
}

fn render() -> std::io::Result<()> {
    let s1 = Box::new(Sphere {
        center: [0.0, 0.0, -1.0],
        radius: 0.5,
    });

    let s2 = Box::new(Sphere {
        center: [0.0, -100.5, -1.0],
        radius: 100.0,
    });

    let world: Hittables = vec![s1, s2];
    let cam: Camera = Camera::default();

    println!("P3\n{} {}\n255\n", IMAGE_WIDTH, IMAGE_HEIGHT);

    for j in (0..IMAGE_HEIGHT).rev() {
        eprintln!("Scanline: {}", j);
        for i in 0..IMAGE_WIDTH {
            let mut pixel_colour: Vec3 = vec_from(0.0);

            for _s in 0..SAMPLES_PER_PIXEL {
                let u = (i as f32 + random_double()) / (IMAGE_WIDTH - 1) as f32;
                let v = (j as f32 + random_double()) / (IMAGE_HEIGHT - 1) as f32;

                let r: Ray = get_ray_from_cam(&cam, u, v);

                add_vector(&mut pixel_colour, &ray_colour(&r, &world, MAX_DEPTH));
            }

            write_colour(&mut std::io::stdout(), pixel_colour, SAMPLES_PER_PIXEL)?;
        }
    }

    Ok(())
}

fn main() -> std::io::Result<()> {
    render()?;

    Ok(())
}
