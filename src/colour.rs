pub mod colour {
    use std::io::{Stdout, Write};

    use crate::{utils::utils::clamp, vec3::vec3::Vec3};

    pub fn write_colour(
        ostream: &mut Stdout,
        colour: Vec3,
        samples_per_pixel: usize,
    ) -> std::io::Result<()> {
        let scale = 1.0 / samples_per_pixel as f32;

        let r = (colour[0] * scale).sqrt();
        let g = (colour[1] * scale).sqrt();
        let b = (colour[2] * scale).sqrt();

        ostream.write(
            format!(
                "{} {} {}\n",
                (256.0 * clamp(r, 0.0, 0.999)) as u32,
                (256.0 * clamp(g, 0.0, 0.999)) as u32,
                (256.0 * clamp(b, 0.0, 0.999)) as u32,
            )
            .as_bytes(),
        )?;

        Ok(())
    }
}
