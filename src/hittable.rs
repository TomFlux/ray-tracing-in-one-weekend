pub mod hittable {
    use crate::ray::ray::{at, Ray};
    use crate::vec3::vec3::*;

    pub struct HitRecord {
        pub p: Vec3,
        pub normal: Vec3,
        pub t: f32,
        pub font_face: bool,
    }

    impl HitRecord {
        fn set_face_normal(&mut self, r: &Ray, outward_normal: &Vec3) {
            self.font_face = dot(&r.direction, &outward_normal) < 0.0;
            self.normal = *outward_normal;

            if !self.font_face {
                mul_by_neg_one(&mut self.normal);
            }
        }

        fn copy(&mut self, o: &HitRecord) {
            self.p = o.p;
            self.normal = o.normal;
            self.t = o.t;
            self.font_face = o.font_face;
        }
    }

    #[inline]
    pub fn empty_hitrecord() -> HitRecord {
        HitRecord {
            p: vec_from(0.0),
            normal: vec_from(0.0),
            t: 0.0,
            font_face: false,
        }
    }

    pub trait Hittable {
        fn hit(&self, r: &Ray, t_min: f32, t_max: f32, rec: &mut HitRecord) -> bool;
    }

    pub struct Sphere {
        pub center: Vec3,
        pub radius: f32,
    }

    impl Hittable for Sphere {
        fn hit(&self, r: &Ray, t_min: f32, t_max: f32, rec: &mut HitRecord) -> bool {
            let mut oc: Vec3 = r.origin;
            sub_vector(&mut oc, &self.center);

            let a = length_squared(&r.direction);
            let half_b = dot(&oc, &r.direction);
            let c = length_squared(&oc) - self.radius.powf(2.0);

            let disc = half_b.powf(2.0) - (a * c);

            if disc < 0.0 {
                return false;
            }

            let disc_sqrt = disc.sqrt();

            let mut root = (-half_b - disc_sqrt) / a;

            if root < t_min || t_max < root {
                root = (-half_b + disc_sqrt) / a;

                if root < t_min || t_max < root {
                    return false;
                }
            }

            rec.t = root;
            rec.p = at(&r, rec.t);

            let mut normal = rec.p;
            sub_vector(&mut normal, &self.center);
            div_vectorf(&mut normal, self.radius);
            rec.normal = normal;

            let mut outward_normal: Vec3 = rec.p;
            sub_vector(&mut outward_normal, &self.center);
            div_vectorf(&mut outward_normal, self.radius);

            rec.set_face_normal(r, &outward_normal);

            true
        }
    }

    pub type Hittables = Vec<Box<dyn Hittable>>;

    pub fn hits(
        hittables: &Hittables,
        r: &Ray,
        t_min: f32,
        t_max: f32,
        rec: &mut HitRecord,
    ) -> bool {
        let mut temp_rec = empty_hitrecord();
        let mut hit_anything = false;
        let mut closest = t_max;

        for object in hittables {
            if object.hit(&r, t_min, closest, &mut temp_rec) {
                hit_anything = true;
                closest = temp_rec.t;
                rec.copy(&temp_rec);
            }
        }

        hit_anything
    }
}
